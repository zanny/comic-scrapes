#!/usr/bin/env python3

# Mostly Lizard includes html formatting in its pages like
# Prequel. There is no real effective way to download that
# into an image format without doing some screen capture
# hijinks. Instead just do the following from the comics dir:
# wget wget --recursive -l 256 --page-requisites --html-extension 
# --convert-links --domains mostlylizard.com mostlylizard.com
