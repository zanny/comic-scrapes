#!/usr/bin/env python3

from bs4 import BeautifulSoup
import os
import sys
import urllib.request

dir= '/ms/Comics/Magical Girl Neil/'
url = 'http://www.magicalgirlneil.com'

# Since pages aren't indexed by numbers but by titles, pages are
# saved in <#>.<title>.<ext> format so we can figure out what
# page we stopped on.

if not os.path.exists(dir):
    os.mkdir(dir)
num = len(os.listdir(dir))

if num > 0:
    # Get latest comic to know its title
    req = urllib.request.urlopen(url)
    soup = BeautifulSoup(req.read().decode('utf-8'), 'lxml')
    title = soup.find('a', class_='comic-nav-comments')['href'].split('/')[-2]
    nums = str(num)

    # Look for the current comic or last comic we grabbed
    for f in os.listdir(dir):
        if os.path.isfile(dir + f):
            parts = f.split('.')
            if title == parts[1]:
                print('No new updates!')
                sys.exit(0)
            elif nums == parts[0]:
                title = parts[1]
                break

    # Grab the next page from the last one we were on
    # We could avoid this by working back from the newest,
    # but I like the file modified times being in newest order
    req = urllib.request.urlopen(url + '/comic/' + title)
    soup = BeautifulSoup(req.read().decode('utf-8'), 'lxml')
    url = soup.find('link', rel='next')['href']
else:
    url += '/comic/magical-girl-neil-1/'

def writer(url, title):
    img = urllib.request.urlopen(url)
    ext = url.split('.')[-1]
    with open(dir + str(num) + '.' + title + '.' + ext, 'wb') as f:
        f.write(img.read())

while True:
    num += 1
    req = urllib.request.urlopen(url)
    soup = BeautifulSoup(req.read().decode('utf-8'), 'lxml')
    title = url.split('/')[4]
    img = soup.find('div', id='comic')
    if img.a:
        writer(img.a.img['src'], title)
    else:
        writer(img.img['src'], title)
        break
    url = img.a['href']
