#!/usr/bin/env python3

from bs4 import BeautifulSoup
import os
import sys
import urllib.request

origin = '.thecomicseries.com'

# Directory to put the comic subdir in, the subdomain of the comic
# (which is used as the comic dir) and a function that takes a soup
# and returns the previous url tag to grab the page count from.
# We need that functor because every comic has a different name for it.
def update(dir, domain, prevtag):
    dir += domain + '/'
    url = 'http://' + domain + origin + '/comics/'
    req = urllib.request.urlopen(url)
    # lxml doesn't work with some sites
    soup = BeautifulSoup(req.read().decode('utf-8'), 'html.parser')
    prev = prevtag(soup)['href']
    count = int(prev.split('/')[-1]) + 1

    if os.path.exists(dir):
        num = len(os.listdir(dir))
        if num == count:
            print('No new updates!')
            sys.exit(0)
    else:
        os.mkdir(dir)
        num = 0

    while num < count:
        num += 1
        page = str(num)
        req = urllib.request.urlopen(url + page)
        soup = BeautifulSoup(req.read().decode('utf-8'), 'html.parser')
        img = soup.find('img', id='comicimage')['src']
        if origin not in img:
            img = 'http://' + domain + origin + img
        req = urllib.request.urlopen(img)
        ext = img.split('.')[-1]
        with open(dir + page + '.' + ext, 'wb') as f:
            f.write(req.read())
