from bs4 import BeautifulSoup
import os
import sys
import urllib.request

dir = '/ms/Comics/Uh Oh Its A Dinosaur/'
curr = 'http://itsadinosaur.com/comic/pk1-00'

def next(page):
    req = urllib.request.urlopen(page)
    soup = BeautifulSoup(req.read().decode('utf-8'), 'lxml')
    return soup.find('a', class_ = 'next-webcomic-link')['href']

def name(url):
    split = url.split('/')
    if split[-1]:
        return split[-1]
    else:
        return split[-2]

if not os.path.exists(dir):
    os.mkdir(dir)
if  len(os.listdir(dir)):
    last = max(os.scandir(dir), key = os.path.getmtime)
    fname = os.path.splitext(os.path.split(last)[-1])[0]
    req = urllib.request.urlopen(curr)
    soup = BeautifulSoup(req.read().decode('utf-8'), 'lxml')
    url = soup.find('a', class_ = 'last-webcomic-link')['href']
    uname = name(url)
    print(uname + ' ' + fname)
    if uname == fname:
        print('No new updates!')
        sys.exit(0)
    curr = next('http://itsadinosaur.com/comic/' + fname)

while True:
    print('Next page: ' + curr)
    peek = next(curr)
    req = urllib.request.urlopen(curr)
    soup = BeautifulSoup(req.read().decode('utf-8'), 'lxml')
    url = soup.find('div', class_ = 'webcomic-image').a.img['src']
    img = urllib.request.urlopen(url)
    ext = url.split('.')[-1]
    with open(dir + name(curr) + '.' + ext, 'wb') as f:
        f.write(img.read())
    if curr == peek:
        break
    curr = peek
