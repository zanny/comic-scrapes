from bs4 import BeautifulSoup
import os
import sys
import urllib.request

dir = '/ms/Comics/Softies/'
domain = 'http://softies.net'
base = domain + '/comic/'

if not os.path.exists(dir):
    os.mkdir(dir)
page = len(os.listdir(dir)) + 1
print('Starting from page: ' + str(page))

while True:
    pagestr = str(page)
    print('Getting page: ' + pagestr)
    req = urllib.request.urlopen(base + pagestr)
    soup = BeautifulSoup(req.read().decode('utf-8'), 'lxml')
    url = soup.find('figure', class_="PageImage").a.img['src']
    img = urllib.request.urlopen(domain + url)
    ext = url.split('.')[-1]
    with open(dir + pagestr + '.' + ext, 'wb') as f:
        f.write(img.read())
    page += 1
