#!/usr/bin/env python3

from bs4 import BeautifulSoup
import os
import sys
import urllib.request

#TODO bugpond has text posts mixed in its pages and there is no tag filtering just the comics
# so if it ever legitimately picks back up this needs to track text posts and skip them

dir= '/ms/Comics/Bugpond/'
root = 'http://bugpond.awgosh.com/page/'

req = urllib.request.urlopen(root + '1')
soup = BeautifulSoup(req.read().decode('utf-8'), 'lxml')
total = int(soup.find('nav', id='pagination').a['href'].split('/')[-1])
print('Total pages: ' + str(total))

if os.path.exists(dir):
    num = len(os.listdir(dir))
    if total == num:
        print('No new updates!')
        sys.exit(0)
    else:
        num = total - num
else:
    os.mkdir(dir)
    num = total

while num > 0:
    page = str(total - num + 1)
    print('On page ' + page)
    req = urllib.request.urlopen(root + str(num))
    soup = BeautifulSoup(req.read().decode('utf-8'), 'lxml')
    url = soup.find('article').img['src']
    img = urllib.request.urlopen(url)
    ext = url.split('.')[-1]
    with open(dir + page + '.' + ext, 'wb') as f:
        f.write(img.read())
    num -= 1
