#!/usr/bin/env python3
from bs4 import BeautifulSoup
from urllib.parse import urljoin
from urllib.request import urlopen
import os
import re

dir = '/ms/Comics/PMDT/'
base = 'http://pmdt.smackjeeves.com/'

if not os.path.exists(dir):
    os.mkdir(dir)
if len(os.listdir(dir)):
    last = max(os.scandir(dir), key = os.path.getmtime)
    name = os.path.splitext(os.path.split(last)[-1])[0]
    print('Most recent page: ' + name)
    req = urlopen(base + 'archive')
    soup = BeautifulSoup(req.read().decode('utf-8'), 'lxml')
    page = soup.find('a', href=re.compile(name))['href']
    req = urlopen(base + page)
    soup = BeautifulSoup(req.read().decode('utf-8'), 'lxml')
    img = soup.find('img', alt='Next >')
    next = img.parent['href']
else:
    next = base + 'comics/555281/pmdt-cover/'

while True:
    print('Next page: ' + next)
    req = urlopen(next)
    soup = BeautifulSoup(req.read().decode('utf-8'), 'lxml')
    comic = soup.find('img', id="comic_image")['src']
    # Some pages don't use absolute image URLs
    comic = urljoin(base, comic)
    # Smackjeeves pages all suffix a / 
    name = next.split('/')[-2]
    ext = comic.split('.')[-1]
    req = urlopen(comic)
    with open(dir + name + '.' + ext, 'wb') as f:
        f.write(req.read())
    img = soup.find('img', alt='Next >')
    new = img.parent['href']
    if next == new:
        print('Out of updates')
        break
    next = new
