#!/usr/bin/env python3
from bs4 import BeautifulSoup
from urllib.parse import urljoin
from urllib.request import urlopen
import os
import re

dir = '/ms/Comics/Latchkey Kingdom/'
base = 'http://latchkeykingdom.smackjeeves.com/'

if not os.path.exists(dir):
    os.mkdir(dir)
if len(os.listdir(dir)):
    last = max(os.scandir(dir), key = os.path.getmtime)
    name = os.path.splitext(os.path.split(last)[-1])[0]
    (num, title) = name.split()
    req = urlopen(base + 'comics/' + num + '/' + title)
    soup = BeautifulSoup(req.read().decode('utf-8'), 'lxml')
    img = soup.find('img', src='http://latchkeykingdom.com/page/img/Button_Next.png')
    next = img.parent['href']
else:
    next = base + 'comics/1818797/usurper-cover/'

while True:
    print('Next page: ' + next)
    req = urlopen(next)
    soup = BeautifulSoup(req.read().decode('utf-8'), 'lxml')
    comic = soup.find('img', id="comic_image")['src']
    # Some pages don't use absolute image URLs
    comic = urljoin(base, comic)
    # Smackjeeves pages all suffix a / 
    name = next.split('/')[-2]
    num = next.split('/')[-3]
    ext = comic.split('.')[-1]
    req = urlopen(comic)
    with open(dir + num + ' ' + name + '.' + ext, 'wb') as f:
        f.write(req.read())
    img = soup.find('img', src='http://latchkeykingdom.com/page/img/Button_Next.png')
    new = img.parent['href']
    if next == new:
        print('Out of updates')
        break
    next = new
