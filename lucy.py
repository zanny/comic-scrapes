from bs4 import BeautifulSoup
import os
import sys
import urllib.request

dest = '/ms/Comics/Lucy'
domain = 'http://lucy.comicsbreak.com/comic/'

if not os.path.exists(dest):
    os.mkdir(dest)
num = len(os.listdir(dest))
print(f"Have {num} pages.")
gap = False

while True:
    num += 1
    url = domain + format(num, '03d')
    print(f"Retrieving page {num} @ {url}")
    try:
        req = urllib.request.urlopen(url)
    # There are gaps in the page count and thus if one
    # is encountered create a dummy file in its spot
    # We wait until the next pass around to check to verify
    # there is a successor after it
    except urllib.error.HTTPError as e:
        print(f"Error code {e.code}")
        if e.code == 404:
            if gap:
                print("Two gaps in a row, considering out of updates")
                break
            gap = True
            continue
        else:
            break
    if gap:
        with open(f"{dest}/Gap {num - 1}", 'w') as f:
            f.write("")
        gap = False
    soup = BeautifulSoup(req.read().decode('utf-8'), 'lxml')
    tag = soup.find('div', id="comic").a.img
    src = tag['src']
    img = urllib.request.urlopen(src)
    ext = src.split('.')[-1]
    with open(f"{dest}/{tag['title']} {num}.{ext}", 'wb') as f:
        f.write(img.read())
    next = soup.select_one(".comic-nav-next")
    if next.name == "span":
        print("Out of updates!")
        break
