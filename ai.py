from bs4 import BeautifulSoup
from urllib.parse import urlparse
from urllib.request import urlopen
import os
import sys

dir = '/ms/Comics/Artificial Incident'
base = 'http://sage.katbox.net/comics/ai/'

if not os.path.exists(dir):
    os.mkdir(dir)
if len(os.listdir(dir)):
    latest = max(os.scandir(dir), key = os.path.getmtime)
    name = os.path.splitext(os.path.split(latest)[-1])[0]
    curr = base + name
    req = urlopen(curr)
    soup = BeautifulSoup(req.read().decode('utf-8'), 'lxml')
    next = soup.find('a', class_ = 'next-webcomic-link')['href']
    if urlparse(curr) == urlparse(next):
        print('No new updates!')
        sys.exit(0)
else:
    next = base + 'issue-one-life-changing'

while True:
    print('Next page: ' + next)
    req = urlopen(next)
    soup = BeautifulSoup(req.read().decode('utf-8'), 'lxml')
    url = soup.find('div', class_ = 'webcomic-image').a.img['src']
    img = urlopen(url)
    if next[-1] == '/':
        name = next.split('/')[-2]
    else:
        name = next.split('/')[-1]
    ext = url.split('.')[-1]
    with open(dir + '/' + name + '.' + ext, 'wb') as f:
        f.write(img.read())
    temp = soup.find('a', class_ = 'next-webcomic-link')['href']
    if urlparse(temp) == urlparse(next):
        print('Out of updates!')
        break
    next = temp
